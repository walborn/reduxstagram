import React from 'react';
import Header from './header';
import Main from './main';

// App
export default () => (
    <div>
        <Header />
        <Main />
    </div>
);
