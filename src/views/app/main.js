import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Pages from 'views/pages';

// Main
export default () =>
    <main>
        <Switch>
            <Route exact path='/' component={Pages.Home}/>
            <Route path='/roster' component={Pages.Roster}/>
            <Route path='/schedule' component={Pages.Schedule}/>
        </Switch>
    </main>