import Home from './home';
import Roster from './roster';
import Schedule from './schedule';

export default { Home, Roster, Schedule };